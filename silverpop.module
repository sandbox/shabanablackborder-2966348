<?php

/**
 * @file
 * Code for the silverpop module.
 */

use Drupal\silverpop\Entity\SilverpopEventType;
use Drupal\silverpop\Event\SilverpopEvent;
use Drupal\silverpop\Event\SilverpopEvents;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Drupal\Component\Utility\Unicode;

/**
 * @file
 * Contains code for the silverpop module.
 */

/**
 * Implements hook_page_attachments().
 *
 * This is where we actually send out the event/page tracking data to Silverpop.
 */
function silverpop_page_attachments(array &$attachments) {
  if (!$enabled_events = _silverpop_get_events_enabled_for_current_page()) {
    return;
  }

  // Page tracking.
  _silverpop_add_page_tracking($attachments);

  // Web tracking.
  _silverpop_add_web_tracking($attachments);

  // Add our js library.
  $attachments['#attached']['library'][] = 'silverpop/silverpop';

  // Event tracking.
  _silverpop_add_event_tracking($attachments, $enabled_events);
}

/**
 * Add Silverpop page tracking.
 *
 * @param array $attachments
 *   The page attachments array.
 */
function _silverpop_add_page_tracking(array &$attachments) {
  $config = \Drupal::service('config.factory')
    ->get('silverpop.admin_settings');
  $domains = $config->get('silverpop_tracked_domains');

  if ($domains) {
    $description = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'com.silverpop.brandeddomains',
        'content' => $domains,
      ],
    ];
    $attachments['#attached']['html_head'][] = [$description, 'description'];
  }
  // Else, if no domains have been set, log an error.
  else {
    \Drupal::logger('silverpop')
      ->error('Silverpop tracked domains have not been set.');
  }
}

/**
 * Add custom page name meta tag for Silverpop web tracking summary report.
 *
 * @param array $attachments
 *   The page attachments array.
 */
function _silverpop_add_web_tracking(array &$attachments) {
  // Get the current page title first.
  $request = \Drupal::request();
  $route = $request
    ->attributes
    ->get(RouteObjectInterface::ROUTE_OBJECT);

  $title = '';
  if ($route) {
    $title = \Drupal::service('title_resolver')->getTitle($request, $route);
  }

  $description = [
    '#tag' => 'meta',
    '#attributes' => [
      'name' => 'com.silverpop.pagename',
      'content' => $title,
    ],
  ];
  $attachments['#attached']['html_head'][] = [$description, 'description'];
}

/**
 * Add event tracking for each enabled event on a page.
 *
 * @param array $attachments
 *   The page attachments array.
 * @param array $enabled_events
 *   An array of silverpop_event_type entities.
 */
function _silverpop_add_event_tracking(array &$attachments, array $enabled_events) {
  foreach ($enabled_events as $event_type) {
    /** @var \Drupal\silverpop\Entity\SilverpopEventType $event_type */
    // An empty CSS selector denotes that this event is a page tracking event.
    if (!$css_selector = $event_type->getCssSelector()) {
      continue;
    }

    // Load the Symfony event dispatcher object through services.
    $dispatcher = \Drupal::service('event_dispatcher');
    // Create our event class object.
    $event = new SilverpopEvent($event_type);
    // Dispatch the event.
    $dispatcher->dispatch(SilverpopEvents::BEFORE_SUBMIT, $event);

    // Process the data that should be sent with this event.
    $service = \Drupal::service('silverpop.silverpop');
    $data = $service->replaceDataTokens($event_type->getData());

    // Finally, add the tracking info in the drupalSettings so our js library
    // can pick it up.
    $tracking_js = [
      'name' => $event_type->label(),
      'type' => $event_type->id(),
      'cssSelector' => $css_selector,
      'data' => $data,
    ];

    $attachments['#attached']['drupalSettings']['silverpop']['events'][$event_type->id()] = $tracking_js;
  }
}

/**
 * Implements hook_library_info_build().
 */
function silverpop_library_info_build() {
  // Load our external silverpop js with the url from silverpop.admin_settings.
  $config = \Drupal::service('config.factory')
    ->get('silverpop.admin_settings');
  $silverpop_js_src = $config->get('silverpop_script_src');

  if (!$silverpop_js_src) {
    return;
  }

  $libraries = [];

  $libraries['silverpop_external_js'] = [
    'version' => '1.x',
    'header' => TRUE,
    'js' => [
      $silverpop_js_src => [
        'type' => 'external',
        'minified' => TRUE,
      ],
    ],
  ];

  return $libraries;
}

/**
 * Fetch event tracking info for the current page.
 *
 * Based on visibility setting this function returns the events that should be
 * tracked in the current page.
 *
 * @return array
 *   The array of silverpop_event_type objects.
 */
function _silverpop_get_events_enabled_for_current_page() {
  static $events_for_page;

  // Cache visibility result if function is called more than once.
  if (isset($events_for_page)) {
    return $events_for_page;
  }

  $events_for_page = [];
  $event_types = \Drupal::service('entity_type.manager')
    ->getStorage('silverpop_event_type')
    ->loadMultiple();

  $path = \Drupal::service('path.current')->getPath();
  $path_alias = Unicode::strtolower(
    \Drupal::service('path.alias_manager')
      ->getAliasByPath($path)
  );
  foreach ($event_types as $event_type) {
    /** @var \Drupal\silverpop\Entity\SilverpopEventType $event_type */
    $visibility_request_path_mode = $event_type->getPageVisibility();
    $visibility_request_path_pages = $event_type->getPageRequestPath();

    // If we don't have any paths, add tracking for this event for this page.
    if (empty($visibility_request_path_pages)) {
      $events_for_page[$event_type->id()] = $event_type;
      continue;
    }

    // Match all paths.
    $page_match = FALSE;
    $paths = preg_split("(\r\n?|\n)", $visibility_request_path_pages);
    foreach ($paths as $key => $path) {
      $paths[$key] = $path === '<front>'
        ? $path
        : '/' . ltrim($path, '/');
    }

    // Check if the current page matches any of the include paths.
    foreach ($paths as $page) {
      $page_match = \Drupal::service('path.matcher')
        ->matchPath($path_alias, $page)
        || (($path != $path_alias)
          && \Drupal::service('path.matcher')->matchPath($path, $page));

      // When $visibility_request_path_mode has a value of 0, the tracking
      // code is displayed on all pages except those listed in $paths. When
      // set to 1, it is displayed only on those pages listed in $paths.
      if ($page_match && $visibility_request_path_mode == SilverpopEventType::PAGE_VISIBILITY_EXCLUDE) {
        $page_match = FALSE;
      }
    }

    if ($page_match) {
      $events_for_page[$event_type->id()] = $event_type;
    }
  }

  return $events_for_page;
}
